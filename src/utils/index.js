import jalaali from "jalaali-js";

export const persian2English = (s) =>
  s.replace(/[۰-۹]/g, (d) => "۰۱۲۳۴۵۶۷۸۹".indexOf(d));

export const sumPrices = (data) => {
  const sell = data.filter((item) => item.category === "sell");
  const buy = data.filter((item) => item.category === "buy");

  const sellSum = sell.reduce((acc, item) => acc + item.price, 0);
  const buySum = buy.reduce((acc, item) => acc + item.price, 0);

  return {
    sellSum,
    buySum,
  };
};

export const numberToCurrency = (number) => number.toLocaleString("ak");

export const convertToJalali = (gregorianDate) => {
  const [datePart] = gregorianDate.split("T");
  const [year, month, day] = datePart.split("-").map(Number);

  const jalaliDate = jalaali.toJalaali(year, month, day + 1);

  return `${jalaliDate.jy}-${jalaliDate.jm
    .toString()
    .padStart(2, "0")}-${jalaliDate.jd.toString().padStart(2, "0")}`;
};

export const convertToJalaliDate = (gregorianDate) => {
  const dateObject = new Date(gregorianDate);

  // Get the year, month, and day in the Gregorian date
  const year = dateObject.getFullYear();
  const month = dateObject.getMonth() + 1; // Months are zero-based, so add 1
  const day = dateObject.getDate();

  // Perform the conversion using jalaali-js
  const jalaliDate = jalaali.toJalaali(year, month, day);

  // Extract the Jalali year, month, and day
  const jalaliYear = jalaliDate.jy;
  const jalaliMonth = jalaliDate.jm;
  const jalaliDay = jalaliDate.jd;

  // Form the Jalali date in the desired format: yyyy-mm-dd
  const convertedDate = `${jalaliYear}-${jalaliMonth
    .toString()
    .padStart(2, "0")}-${jalaliDay.toString().padStart(2, "0")}`;

  return convertedDate;
};
