import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Users from "./Users/Users";
import Expense from "./Expense/Expense";
import NotFound from "./not-found/NotFound";
import Navigation from "../components/UI/Navigation";
import PageHeader from "../components/UI/PageHeader";

const App = () => {
  return (
    <>
      <PageHeader />
      <Router>
        <Routes>
          <Route path="/" element={<Expense />} />
          <Route path="/users" element={<Users />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
        <Navigation />
      </Router>
    </>
  );
};

export default App;
