import { useState, useEffect, Fragment } from "react";
import dayjs from "dayjs";
import isBetween from "dayjs/plugin/isBetween";
import { Grid, FormControl } from "@mui/material";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import AddCardIcon from "@mui/icons-material/AddCard";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import AddExpense from "./AddExpense";
import Chip from "@mui/material/Chip";
import Divider from "@mui/material/Divider";
import {
  sumPrices,
  numberToCurrency,
  convertToJalali,
  convertToJalaliDate,
} from "../../utils";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import DeleteIcon from "@mui/icons-material/Delete";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { AdapterDateFnsJalali } from "@mui/x-date-pickers/AdapterDateFnsJalali";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import isSameOrAfter from "dayjs/plugin/isSameOrAfter";
import isSameOrBefore from "dayjs/plugin/isSameOrBefore";

dayjs.extend(isSameOrAfter);
dayjs.extend(isBetween);
dayjs.extend(isSameOrBefore);

const Expense = () => {
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [open, setOpen] = useState(false);
  const [popupOpen, setPopupOpen] = useState(false);
  const [expenses, setExpenses] = useState([]);
  const [report, setReport] = useState("گزارش کامل");
  const { sellSum, buySum } = sumPrices(expenses);

  const filterDate = () => {
    const filteredData = expenses.filter(({ date }) => {
      if (startDate && endDate) {
        setReport(
          `${convertToJalaliDate(startDate)} - ${convertToJalaliDate(endDate)}`
        );
        return dayjs(date).isBetween(startDate, endDate, "day", "[]");
      } else if (startDate && !endDate) {
        setReport(`${convertToJalaliDate(startDate)} از `);
        return dayjs(date).isSameOrAfter(startDate, "day");
      } else if (!startDate && endDate) {
        setReport(`${convertToJalaliDate(endDate)} تا `);
        return dayjs(date).isSameOrBefore(endDate, "day");
      } else {
        return null;
      }
    });
    setExpenses(filteredData);
    setStartDate(null);
    setEndDate(null);
    setPopupOpen(false);
  };

  const getExpenses = async () => {
    const response = await fetch(process.env.REACT_APP_API_URL + "/expenses");
    return await response.json();
  };

  const dailyReportClick = () => {
    const filteredData = expenses.filter(({ date }) => {
      const filteredDate = dayjs(date).format("YYYY-MM-DD");
      return dayjs().isSame(filteredDate, "day");
    });
    setExpenses(filteredData);
    setReport("گزارش روزانه");
  };

  const dailyReportDelete = () => {
    getExpenses().then(setExpenses);
    setReport("گزارش کامل");
  };

  const removeExpense = (id) => {
    const url = process.env.REACT_APP_API_URL + "/deleteExpense";
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ id }),
    }).then((res) => {
      res.json().then(() => {
        window.location.reload(false);
      });
    });
  };

  useEffect(() => {
    getExpenses().then(setExpenses);
  }, []);

  return (
    <div>
      <Grid
        container
        spacing={1}
        alignItems="center"
        marginTop={1}
        marginBottom={1}
      >
        <Grid item xs={5}>
          <Chip
            sx={{ width: "100%" }}
            label="گزارش دلخواه"
            onClick={() => setPopupOpen(true)}
            onDelete={dailyReportDelete}
          />
        </Grid>
        <Grid item xs={5}>
          <Chip
            sx={{ width: "100%" }}
            label="گزارش روزانه"
            onClick={dailyReportClick}
            onDelete={dailyReportDelete}
          />
        </Grid>
        <Grid item xs={2}>
          <IconButton
            aria-label="open"
            color="primary"
            onClick={() => setOpen(true)}
          >
            <AddCardIcon />
          </IconButton>
        </Grid>
      </Grid>
      <Divider textAlign="center" />
      <Grid container spacing={2} padding={1}>
        <Grid item xs={12}>
          <Chip
            label={<Typography variant="h6">{report}</Typography>}
            color="primary"
            sx={{ width: "100%", height: "40px" }}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={6}>
          <Chip
            icon={<AddIcon />}
            label={
              <Typography variant="h6">{numberToCurrency(sellSum)}</Typography>
            }
            color="success"
            sx={{ width: "100%", height: "40px" }}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={6}>
          <Chip
            icon={<RemoveIcon />}
            label={
              <Typography variant="h6">{numberToCurrency(buySum)}</Typography>
            }
            color="error"
            sx={{ width: "100%", height: "40px" }}
            variant="outlined"
          />
        </Grid>
      </Grid>
      <Divider textAlign="center" sx={{ marginBottom: "8px" }} />

      {expenses.map((expense) => {
        const day = convertToJalali(expense.date);
        return (
          <Fragment key={expense._id}>
            <ListItem
              sx={{
                bgcolor: expense.category === "buy" ? "#fa8f8c" : "#9ed392",
                borderRadius: "5px",
                marginBottom: "5px",
              }}
            >
              <ListItemText
                primary={
                  <Stack
                    spacing={{ xs: 1, sm: 2 }}
                    direction="row"
                    useFlexGap
                    flexWrap="wrap"
                    alignItems="center"
                    justifyContent="space-between"
                  >
                    <Typography sx={{ fontSize: "15px", fontWeight: "bold" }}>
                      {expense.category === "buy" ? "- " : "+ "}
                      {numberToCurrency(expense.price)}
                    </Typography>
                    <IconButton
                      size="small"
                      color="error"
                      onClick={() => removeExpense(expense._id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </Stack>
                }
                secondary={
                  <Stack
                    spacing={{ xs: 1, sm: 2 }}
                    direction="row"
                    useFlexGap
                    flexWrap="wrap"
                    justifyContent="space-between"
                  >
                    <div>{expense.description}</div>
                    <div>{day}</div>
                  </Stack>
                }
              />
            </ListItem>
          </Fragment>
        );
      })}
      <Dialog
        open={popupOpen}
        onClose={() => setPopupOpen(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title" dir="rtl">
          فیلتر مورد نظر خود را انتخاب کنید
        </DialogTitle>
        <DialogContent>
          <FormControl required>
            <LocalizationProvider dateAdapter={AdapterDateFnsJalali}>
              <DemoContainer
                sx={{ alignItems: "center" }}
                components={["DatePicker"]}
              >
                <DatePicker
                  label="تاریخ شروع "
                  onChange={(e) => setStartDate(e ?? null)}
                />
                <DatePicker
                  label="تاریخ پایان "
                  onChange={(e) => setEndDate(e ?? null)}
                />
              </DemoContainer>
            </LocalizationProvider>
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setPopupOpen(false)}>بستن پنجره</Button>
          <Button onClick={filterDate} color="success">
            اعمال فیلتر
          </Button>
        </DialogActions>
      </Dialog>
      <Drawer
        sx={{ minWidth: "200px" }}
        anchor="right"
        open={open}
        onClose={() => {
          setOpen(false);
          setStartDate(null);
          setEndDate(null);
        }}
      >
        <AddExpense onClose={() => setOpen(false)} />
      </Drawer>
    </div>
  );
};

export default Expense;
