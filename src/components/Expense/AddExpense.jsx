import { useState } from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { FormControl, Grid } from "@mui/material";
import Radio from "@mui/material/Radio";
import Divider from "@mui/material/Divider";
import RadioGroup from "@mui/material/RadioGroup";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormLabel from "@mui/material/FormLabel";
import { AdapterDateFnsJalali } from "@mui/x-date-pickers/AdapterDateFnsJalali";

const AddExpense = ({ onClose }) => {
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(null);
  const [date, setDate] = useState();
  const [category, setCategory] = useState("sell");
  const [payment, setPayment] = useState("card");

  const addNewExpense = (e) => {
    e.preventDefault();
    const url = process.env.REACT_APP_API_URL + "/addExpense";
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ description, price, date, category, payment }),
    }).then((res) => {
      res.json().then(() => {
        onClose();
        window.location.reload(false);
      });
    });
  };

  return (
    <FormControl required>
      <Box component="form" padding={2} fullWidth>
        <FormLabel
          required={false}
          fullWidth
          id="demo-row-radio-buttons-group-label"
        >
          دسته بندی سند مالی
        </FormLabel>
        <RadioGroup
          fullWidth
          value={category}
          aria-labelledby="demo-row-radio-buttons-group-label"
          name="row-radio-buttons-group"
          onChange={(e) => setCategory(e.target.value)}
        >
          <FormControlLabel value="sell" control={<Radio />} label="فروش" />
          <FormControlLabel value="buy" control={<Radio />} label="خرید جنس" />
        </RadioGroup>
        <Divider textAlign="center" sx={{ margin: "16px 0" }} />

        <FormLabel
          required={false}
          fullWidth
          id="demo-row-radio-buttons-group-label"
        >
          روش پرداخت
        </FormLabel>
        <RadioGroup
          fullWidth
          value={payment}
          aria-labelledby="demo-row-radio-buttons-group-label"
          name="row-radio-buttons-group"
          onChange={(e) => setPayment(e.target.value)}
        >
          <FormControlLabel
            value="card"
            control={<Radio />}
            label="کارت"
            defaultChecked
          />
          <FormControlLabel value="cash" control={<Radio />} label="پول نقد " />
        </RadioGroup>
        <Divider textAlign="center" sx={{ margin: "16px 0" }} />
        <LocalizationProvider dateAdapter={AdapterDateFnsJalali}>
          <DemoContainer components={["DatePicker"]}>
            <DatePicker label="تاریخ" onChange={(e) => setDate(e ?? null)} />
          </DemoContainer>
        </LocalizationProvider>
        <TextField
          fullWidth
          id="price"
          label="مبلغ"
          variant="outlined"
          type="number"
          margin="dense"
          onChange={(e) => setPrice(e.target.value)}
        />
        <TextField
          fullWidth
          id="description"
          label="توضیحات"
          variant="outlined"
          margin="dense"
          onChange={(e) => setDescription(e.target.value)}
        />

        <Box marginTop={2} fullWidth>
          <Grid container spacing={2}>
            <Grid item xs={9}>
              <Button
                size="medium"
                variant="contained"
                type="submit"
                fullWidth
                onClick={addNewExpense}
                disabled={!price || !date}
              >
                افزودن سند مالی
              </Button>
            </Grid>
            <Grid item xs={3}>
              <Button
                size="medium"
                variant="contained"
                type="submit"
                color="error"
                onClick={(e) => {
                  e.preventDefault();
                  onClose();
                }}
                fullWidth
              >
                بستن
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </FormControl>
  );
};
export default AddExpense;
