import { useState, useEffect, Fragment } from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import IconButton from "@mui/material/IconButton";
import AddBoxIcon from "@mui/icons-material/AddBox";
import PersonAddAlt1Icon from "@mui/icons-material/PersonAddAlt1";
import Badge from "@mui/material/Badge";
import CoffeeIcon from "@mui/icons-material/Coffee";
import TextField from "@mui/material/TextField";
import { Grid } from "@mui/material";
import IndeterminateCheckBoxIcon from "@mui/icons-material/IndeterminateCheckBox";
import DeleteIcon from "@mui/icons-material/Delete";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

import AddNewUser from "./AddNewUser";

const Users = () => {
  const [open, setOpen] = useState(false);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [userId, setUserId] = useState(null);
  const [users, setUsers] = useState([]);
  const [search, setSearch] = useState("");

  const getUsers = async () => {
    const response = await fetch(process.env.REACT_APP_API_URL + "/users");
    return await response.json();
  };

  useEffect(() => {
    if (search) {
      const filteredData = users.filter((user) => {
        if (
          user.name.includes(search) ||
          user.lastName.includes(search) ||
          user.phone.includes(search)
        )
          return user;
      });
      setUsers(filteredData);
    } else {
      getUsers().then(setUsers);
    }
  }, [search]);

  const addCoffee = (id) => {
    const url = process.env.REACT_APP_API_URL + "/addCoffee";
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ id }),
    }).then((res) => {
      res.json().then(() => {
        window.location.reload(false);
      });
    });
  };

  const minCoffee = (id) => {
    const url = process.env.REACT_APP_API_URL + "/minCoffee";
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ id }),
    }).then((res) => {
      res.json().then(() => {
        window.location.reload(false);
      });
    });
  };

  const removeUser = () => {
    const url = process.env.REACT_APP_API_URL + "/deleteUser";
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ id: userId }),
    }).then((res) => {
      res.json().then(() => {
        setDialogOpen(false);
        setUserId(null);
        window.location.reload(false);
      });
    });
  };

  return (
    <>
      <Grid
        container
        spacing={1}
        alignItems="center"
        marginTop={1}
        marginBottom={1}
      >
        <Grid item xs={10}>
          <TextField
            sx={{ marginLeft: "20px", width: "90%" }}
            id="search"
            label="جستجو"
            variant="outlined"
            margin="dense"
            size="small"
            onChange={(e) => setSearch(e.target.value)}
          />
        </Grid>
        <Grid item xs={2}>
          <IconButton
            aria-label="open"
            color="primary"
            onClick={() => setOpen(true)}
          >
            <PersonAddAlt1Icon />
          </IconButton>
        </Grid>
      </Grid>

      <List
        sx={{
          width: "100%",
          maxWidth: 360,
        }}
      >
        <Divider textAlign="center" />
        {users.map((user) => {
          return (
            <Fragment key={user._id}>
              <ListItem
                sx={{
                  borderRadius: "5px",
                }}
              >
                <ListItemText
                  primary={
                    <Stack
                      spacing={{ xs: 1, sm: 2 }}
                      direction="row"
                      useFlexGap
                      flexWrap="wrap"
                      justifyContent="space-between"
                    >
                      <Typography
                        marginTop={0.8}
                        sx={{ fontSize: "15px", fontWeight: "bold" }}
                      >{`${user.name} ${user.lastName}`}</Typography>
                      <div>
                        <IconButton
                          size="small"
                          aria-label="add"
                          color="primary"
                          onClick={() => minCoffee(user._id)}
                          disabled={user.numberOfCoffee === 0}
                        >
                          <IndeterminateCheckBoxIcon />
                        </IconButton>
                        <Badge
                          size="small"
                          color="warning"
                          showZero
                          badgeContent={user.numberOfCoffee}
                          sx={{ marginRight: "10px", marginLeft: "10px" }}
                        >
                          <CoffeeIcon />
                        </Badge>
                        <IconButton
                          size="small"
                          aria-label="add"
                          color="primary"
                          onClick={() => addCoffee(user._id)}
                        >
                          <AddBoxIcon />
                        </IconButton>
                        <IconButton
                          size="small"
                          aria-label="add"
                          color="error"
                          onClick={() => {
                            setDialogOpen(true);
                            setUserId(user._id);
                          }}
                        >
                          <DeleteIcon />
                        </IconButton>
                      </div>
                    </Stack>
                  }
                  secondary={
                    <Stack
                      spacing={{ xs: 1, sm: 2 }}
                      direction="row"
                      useFlexGap
                      flexWrap="wrap"
                      alignItems="center"
                      justifyContent="space-between"
                    >
                      <div>{user.phone}</div>
                    </Stack>
                  }
                />
              </ListItem>
              {/* <ListItem>
                <ListItemText
                  primary={
                    <>
                      <Chip
                        icon={<FaceIcon />}
                        label={`${user.name} ${user.lastName}`}
                      />
                      <IconButton
                        aria-label="add"
                        color="error"
                        onClick={() => {
                          setDialogOpen(true);
                          setUserId(user._id);
                        }}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </>
                  }
                  secondary={
                    <Chip
                      color="info"
                      sx={{ marginTop: "5px" }}
                      icon={<PhoneIphoneRoundedIcon />}
                      label={user.phone}
                    />
                  }
                />
                <IconButton
                  aria-label="add"
                  color="primary"
                  onClick={() => minCoffee(user._id)}
                  disabled={user.numberOfCoffee === 0}
                >
                  <IndeterminateCheckBoxIcon />
                </IconButton>
                <Badge
                  color="warning"
                  showZero
                  badgeContent={user.numberOfCoffee}
                  sx={{ marginRight: "10px", marginLeft: "10px" }}
                >
                  <CoffeeIcon />
                </Badge>
                <IconButton
                  aria-label="add"
                  color="primary"
                  onClick={() => addCoffee(user._id)}
                >
                  <AddBoxIcon />
                </IconButton>
              </ListItem> */}
              <Divider textAlign="center" />
              <Dialog
                open={dialogOpen}
                onClose={() => setDialogOpen(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-title" dir="rtl">
                  آیا می خواهی مشتری را حذف کنی؟
                </DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description" dir="rtl">
                    وقتی مشتری حذف شود دیگه به اطلاعاتش دسترسی نداری!
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={() => setDialogOpen(false)}>
                    بستن پنجره
                  </Button>
                  <Button
                    onClick={() => removeUser(user._id)}
                    autoFocus
                    color="error"
                  >
                    حذف مشتری
                  </Button>
                </DialogActions>
              </Dialog>
            </Fragment>
          );
        })}
      </List>

      <Drawer anchor="right" open={open} onClose={() => setOpen(false)}>
        <AddNewUser onClose={() => setOpen(false)} />
      </Drawer>
    </>
  );
};

export default Users;
