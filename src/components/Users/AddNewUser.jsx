import { useState } from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { FormControl, Grid } from "@mui/material";

const AddNewUser = ({ onClose }) => {
  const [name, setName] = useState("");
  const [lastName, setLastName] = useState("");
  const [phone, setPhone] = useState("");

  const addNewUser = (e) => {
    e.preventDefault();
    const url = process.env.REACT_APP_API_URL + "/addNewUser";
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ name, lastName, phone }),
    }).then((res) => {
      res.json().then(() => {
        onClose();
        window.location.reload(false);
      });
    });
  };

  return (
    <FormControl required>
      <Box component="form" padding={2} fullWidth>
        <TextField
          id="name"
          label="نام"
          variant="outlined"
          margin="dense"
          fullWidth
          onChange={(e) => setName(e.target.value)}
        />
        <TextField
          fullWidth
          id="lastName"
          label="نام خانوادگی"
          variant="outlined"
          margin="dense"
          onChange={(e) => setLastName(e.target.value)}
        />
        <TextField
          id="phone"
          label="شماره تلفن "
          variant="outlined"
          fullWidth
          margin="dense"
          onChange={(e) => setPhone(e.target.value)}
        />
        <Box marginTop={2}>
          <Grid container spacing={2}>
            <Grid item xs={9}>
              <Button
                size="medium"
                variant="contained"
                type="submit"
                onClick={addNewUser}
                fullWidth
                disabled={!name || !lastName || !phone}
              >
                افزودن مشتری
              </Button>
            </Grid>
            <Grid item xs={3}>
              <Button
                size="medium"
                variant="contained"
                type="submit"
                color="error"
                onClick={(e) => {
                  e.preventDefault();
                  onClose();
                }}
                fullWidth
              >
                بستن
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </FormControl>
  );
};
export default AddNewUser;
