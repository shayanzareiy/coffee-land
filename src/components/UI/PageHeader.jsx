import Divider from "@mui/material/Divider";
import { Paper, Typography } from "@mui/material";

const PageHeader = () => {
  return (
    <>
      <Paper
        sx={{ padding: "10px", background: "#ee9a66", textAlign: "center" }}
      >
        <Typography variant="h6" component="span">
          کافی لند
        </Typography>
      </Paper>
      <Divider textAlign="center" marginTop={1} />
    </>
  );
};

export default PageHeader;
