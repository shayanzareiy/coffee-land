import { useState } from "react";
import { useNavigate } from "react-router-dom";

import Box from "@mui/material/Box";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import GroupIcon from "@mui/icons-material/Group";
import AccountBalanceWalletIcon from "@mui/icons-material/AccountBalanceWallet";

const Navigation = () => {
  const { pathname } = window.location;
  const navigate = useNavigate();
  const [value, setValue] = useState(pathname === "/" ? 0 : 1);

  return (
    <Box
      sx={{
        position: "fixed",
        bottom: 0,
        left: 0,
        right: 0,
      }}
    >
      <BottomNavigation
        showLabels
        value={value}
        onChange={() => setValue(pathname === "/" ? 1 : 0)}
      >
        <BottomNavigationAction
          label="حسابداری"
          icon={<AccountBalanceWalletIcon />}
          onClick={() => navigate("/")}
        />
        <BottomNavigationAction
          label="باشگاه مشتریان"
          icon={<GroupIcon />}
          onClick={() => navigate("/users")}
        />
      </BottomNavigation>
    </Box>
  );
};

export default Navigation;
